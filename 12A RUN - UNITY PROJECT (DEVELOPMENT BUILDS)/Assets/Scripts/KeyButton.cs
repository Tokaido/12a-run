﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyButton : MonoBehaviour {

    public Button button;
	// Use this for initialization
	void Start () {
        button = GetComponent<Button>();
	}
	
	// Update is called once per frame
	void Update () {
        if (GameControl.instance.gameOver == true) button.interactable = false;
        else if (Input.GetKeyDown(KeyCode.Escape)) button.onClick.Invoke();
	}
}
