﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Danger : MonoBehaviour {

    

    private void Start()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.GetComponent<Player>()!=null)
        {
            
            GameControl.instance.gameOver = true;
        }
    }
    private void Update()
    {
        if (transform.position.x < -20) Destroy(gameObject);
    }
}
