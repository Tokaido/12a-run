﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButtons : MonoBehaviour {

	public void OnMenuClick(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }
}
