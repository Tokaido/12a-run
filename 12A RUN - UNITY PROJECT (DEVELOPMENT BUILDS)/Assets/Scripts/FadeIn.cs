﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeIn : MonoBehaviour {

    private Image image;
    private GameObject imageobj;
    private float alpha=1.0f;
    public float startTime;
    private float t;
    public float duration = 2f;
    
    void Start()
    {
        image = GetComponent<Image>();
        imageobj = GetComponent<GameObject>();
        startTime = Time.time;
    }
    void Update()
    {
        t = (startTime - Time.time) / duration;
        alpha = Mathf.SmoothStep(1.0f, 0.0f, -t);
        image.color = new Color(1f, 1f, 1f, alpha);
        if (alpha == 0f) Destroy(imageobj);
    }
}
