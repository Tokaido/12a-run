﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public static Player instance;
    public float UpForce = 200;

	private bool isDead = false;
    private Rigidbody2D rb2d;
    private Animator anim;
    private int jumpCount=0;
	
	void Start ()
    {
        rb2d = GetComponent <Rigidbody2D>();
        anim = GetComponent <Animator>();
        
	}

	void Update () 
	{
        
        if (GameControl.instance.gameOver == true) { isDead = true; }
        if (GameControl.instance.isPaused == true) { anim.enabled = false; rb2d.velocity = Vector2.zero; rb2d.gravityScale = 0; }
        else if (anim.enabled == false) { rb2d.gravityScale = 5; anim.enabled = true; }
        if (isDead == false && GameControl.instance.isPaused == false)
        {
            anim.SetFloat("AnimRunSpeed", Mathf.Sqrt(-GameControl.instance.scrollSpeed / 10));
            foreach(Touch touch in Input.touches) {
                if(touch.phase==TouchPhase.Began)
                if (touch.position.x < Screen.width - 60 || touch.position.y < Screen.height - 60)
                {
                    if (jumpCount < 2)
                    {
                        rb2d.velocity = Vector2.zero;
                        rb2d.AddForce(new Vector2(0f, UpForce));
                        anim.SetBool("isJumping", true);
                        anim.SetBool("onGround", false);

                        jumpCount++;
                    }
                }
            }
            if(Input.GetKeyDown(KeyCode.Space))
                if (jumpCount < 2)
                {
                    rb2d.velocity = Vector2.zero;
                    rb2d.AddForce(new Vector2(0f, UpForce));
                    anim.SetBool("isJumping", true);
                    anim.SetBool("onGround", false);

                    jumpCount++;
                }

        }
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.GetComponent<Danger>()!=null&&isDead==false)
        {
            anim.SetTrigger("isDeadT");
        }
    }
    
    private void OnCollisionEnter2D()
    {
        if (isDead == false)
        {
            anim.SetBool("onGround", true);
            anim.SetBool("isJumping", false);
            jumpCount = 0;
        }
    } 
}
