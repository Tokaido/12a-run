﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

    public Transform[] spawnPoints;
    public float spawnTime = 3f;
    public GameObject enemy;
    // Use this for initialization
    void Start () {
        InvokeRepeating("Spawn", spawnTime, spawnTime);
    }
	
    void Spawn()
    {
        if (GameControl.instance.gameOver||GameControl.instance.isPaused)
        {
            return;
        }
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        
        Instantiate(enemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
    }
	// Update is called once per frame
	void Update () {
		
	}
}
