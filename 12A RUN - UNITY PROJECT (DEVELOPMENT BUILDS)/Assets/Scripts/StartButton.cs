﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class StartButton : MonoBehaviour {
    public int quitTap = 0;
    private float lastTimePressed;
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            quitTap++;
            lastTimePressed = Time.time;
            
        }
        if (Time.time - lastTimePressed > 1) quitTap = 0;
        if (quitTap == 2) Application.Quit();
    }
 
	public void OnStartClick(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }
}
