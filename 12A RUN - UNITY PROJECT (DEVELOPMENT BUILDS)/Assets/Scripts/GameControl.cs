﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour 
{
	public static GameControl instance;			
	public Text scoreText;						
	public GameObject gameOvertext;
    public GameObject menuPanel;
    public float score = 0f;

    public float currentScrollSpeed=-10f;
    public float targetScrollSpeed=-10f;
    
    private double speedMilestone=0;
    private double timeCount=0;						
	public bool gameOver = false;
    public bool isPaused = false;				
	public float scrollSpeed=-10f;
    public float startTime;
    private float t;
    public float duration=5f;
    

	void Awake()
	{
		if (instance == null)
			instance = this;
		else if(instance != this)
			Destroy (gameObject);
	}
    

	void Update()
	{   if (!gameOver&&!isPaused)
        {
            
            if (timeCount >= 100)
            {
                score += 1; timeCount = 0;
                SetScore();
            }
            else timeCount+=-scrollSpeed;
            if (score > speedMilestone) { currentScrollSpeed = scrollSpeed; targetScrollSpeed = scrollSpeed - 3f; speedMilestone += 100;startTime = Time.time; }
            t = (startTime - Time.time) / duration;
            scrollSpeed = Mathf.SmoothStep(currentScrollSpeed, targetScrollSpeed,-t);
        }
        if (gameOver && !menuPanel.activeSelf) { menuPanel.SetActive(true); }
	}
   
    public void OnPauseClick()
    {
        if (isPaused == true) isPaused = false;
        else isPaused = true;
    }
    void SetScore()
    {
        scoreText.text=score.ToString();
    }
}
